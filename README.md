# pwn-scrape
Pwn-scrape allows you to check for breaches on haveibeenpwned whithout api, do not abuse this!

Version 2.0.0

# Installing deps:

### make sure you have the latest version of gekodriver and have it installed in your path!

Go download geckodriver and put it your path `/usr/local/bin`
https://github.com/mozilla/geckodriver/releases
`pip install --user -r requirements.txt` 

### Usage

Show help

python pwn.py -h

To search a single email

`python pwn.py -e test@test.com`

To search from a list of emails

`python pwn.py -l /path/to/file`

If you get no output or notice the browser is showing a cloudlfare error, then you need to run the sneaky flag.

`python pwn.py -e test@test.com -s or python pwn.py -l /path/to/list -s`

There is also a Delay flag you can use it by setting

`-d`

